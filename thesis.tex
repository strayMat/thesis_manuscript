\documentclass{report}


\usepackage{arxiv}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{makecell}
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{lipsum}
\usepackage{graphicx}
\usepackage[dvipsnames]{xcolor}
\usepackage[natbib=true,style=authoryear,maxnames=999,maxcitenames=2]{biblatex}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage{pifont}
\usepackage{csvsimple}

% notes in the margin
\usepackage{todonotes}
\usepackage{snaptodo}

\setlength{\marginparwidth}{1.4cm}
\setlength{\marginparsep}{-.01cm}
%\setlength\textwidth{177.8mm}

\snaptodoset{block rise=1em}
\snaptodoset{margin block/.style={font=\tiny}} \newcommand{\gv}[1]{%
    \snaptodo[margin block/.append style=green!50!black]{%
	\sloppy\textbf{Gael}: #1}}
\snaptodoset{margin block/.style={font=\tiny}} \newcommand{\md}[1]{%
    \snaptodo[margin block/.append style=blue]{%
	\sloppy\textbf{Matthieu}: #1}}
  
% color commands
\colorlet{P}{ForestGreen}
\colorlet{I}{MidnightBlue}
\colorlet{C}{YellowOrange}
\colorlet{O}{DarkOrchid}

% equations commands
\newcommand{\indep}{\perp \!\!\! \perp}
\newtheorem{assumption}{Assumption}


\bibliography{references} 


\title{
  Representations and inference from time-varying routine care data 
}

\author{Matthieu Doutreligne}
\date{}

\begin{document}
\maketitle

\begin{abstract}

  Real World Databases are increasingly accessible, exhaustive and with
  fine temporal details. Unlike traditional data used in clinical research, they
  describe the routine organization of care. These day-to-day records of
  patients care enable new research questions, notably concerning the efficiency
  of interventions after market access, the heterogeneity of their benefits in
  under-served populations or the development of personalized medicine. On the
  other hand, the complexity and large-scale nature of these databases pose a
  number of challenges for effectively answering these questions. To remedy
  these problems, econometricians and epidemiologists have recently proposed
  the use of flexible models combining causal inference with high-dimensional
  machine learning.

  Chapter 1 uses a national case study of the 32 French regional and
  university hospitals to highlight key aspects of modern Clinical Data
  Warehouses (CDWs), the cornerstone infrastructure of precision medicine. From
  semi-structured interviews and an analysis of declared observational studies
  on CDWs in France, I highlight both the current potential and challenges of
  leveraging these routinely collected data for research purposes.

  Acknowledging the difficulty to access large sample sizes and computational
  power to develop generalizable predictive models, Chapter 2 leverages
  distributional representations of medical concepts for clinical tasks. I show
  that these sharable representations out-perform more complex models for
  small sample datasets.

  Despite a current focus on artificial intelligence, healthcare does not seek
  purely predictive models, but appropriate interventions that will benefit
  specific patients. This is a decision making problem highly amenable to
  counterfactual prediction rather than statistical learning. Chapter 3 emulates
  a clinical trial to evaluate an intervention in the intensive care unit. I
  highlight the importance of the different practical choices when developing
  counterfactual prediction algorithms on time-varying data collected in routine
  care.

  In high-dimensional settings such as time-varying data and heterogeneity of
  interventions on subgroups, the selection of hyper-parameters for the causal
  model is crucial to avoid under- or over-learning. Chapter 4 demonstrates the
  fragility of the usual evaluation metrics (Mean Squared Error) for
  counterfactual predictions. I highlight the performance of the doubly robust
  R-risk over other existing risks and discuss their effectiveness regimes in a
  simulation and three semi-simulated datasets.

  Chapter 5 concludes by highlighting the potential of combining machine
  learning methods and routine care data to shed light on current public health
  issues. I discuss new avenues to improve the development and the evaluation of
  tailored interventions, public health policies or quality-of-care indicators.

\end{abstract}
% keywords can be removed
%\keywords{First keyword \and Second keyword \and More}

\tableofcontents




\chapter{Introduction: amazing opportunities of health data?}\label{chap:intro}
\section{How I came into this landscape}\label{sec:intro:landscape}


I will present my progression among these opportunities.

\subsection{Increasing collection of data}


% RCT and Real world data
\paragraph{Recent surge in the collection of healtchare data, are drawing
  increasing attention to observational studies based on nonrandomized data
  sources for estimating causal effects.} Often referred as Real World Data
(RWD), they come from routine care Information System routine such as
Electronic Health Records (EHR) or insurance claims \citep{wang2023emulation}.
RWD are interesting opportunities to research effectiveness of treatments in
routine care data, generalize RCT findings to a broader population, to explore
treatment heterogeneity on subgroups \citep{mant1999can, desai2021broadening},
and to gain first insights at smaller costs than RCTs
\citep{black1996we,bosdriesz2020evidence}. Moreover, this data is well suited
to measure the impact of health policies or changes in professional
guidelines. These opportunities have been well identified by HTA agencies such
as the \textit{Food and Drug Administration}
\citep{desai2021broadening,fda_real-world_2021}, the \textit{European
  Medicines Agency} \citep{flynn_marketing_2022}, the French \textit{Haute
  Autorité de Santé} \citep{vanier2023rapid} and the British \textit{National
  Institute for
  Health and Care Excellence} \citep{kent_nice_2022}.

%
Randomized Control Trials (RCT) have
long been the gold standard to measure whether the benefits of an intervention
outweigh its harms \citep{brook1986method}. However, routinely collected data
such as Electronic Health Records (EHR) or insurance claims
\citep{wang2023early} can also be leveraged to estimate treatment effects via
counterfactual prediction analyses \citep{hernan2019second}.  They are
particularly useful to study comparative effectiveness of treatments,
generalize RCT findings to broader populations, to explore treatment
heterogeneity on subgroups \citep{mant1999can,desai2021broadening}, and to
gain first insights at smaller costs than RCTs
\citep{black1996we,bosdriesz2020evidence}. However, the lack of controlled
interventions and the time-varying nature of such data open many threats to
the validity of causal inference \citep{bouvier2023should}.
Despite the interest in RWD for observational studies, a clear analysis
framework is required to avoid the inherent pitfalls of such studies, limit the
introduction of biases and derive insightful evidences \citep{hernan2019second,
  wang2023emulation}. It has been shown that failure to follow closely a ground
truth emulated trial,
yields substantially different results in observational studies: eg. for
cardiovascualar outcomes of antidiabetic or antiplatelet medications based on
insurance claims data \citep{schneeweiss2021conducting}. Comparing and
replicating real-world evidence studies for focused on the risk of major
bleeding
with dabigatran compared with warfarin, \cite{wang2022understanding} found that
60 to 88\% of variation remained unexplained. On the contrary, when closely
following the RCTs protocol, real-world evidence studies has been shown to be
highly consistent with RCTs \citep{wang2023emulation}.



\section{The data: Electronic Health Records}\label{sec:intro:data}

\subsection{Various types of data: Real World Data, observational data}\label{subsec:intro:real_world_data}

Real world data refer to routinely collected data
Gradient and distinction between research data and opportunistic data collection.

Exemples:
hand made data collection for one research question with a specific protocol,
specialized registry and cohorts
routine data collection


\subsection{Interventional data vs observational
  data}\label{subsec:intro:interventional_vs_observational}


\textit{In an experiment, the reason for the exposure assignment is solely to
  suit the objectives of the study; if people receive their exposure assignment
  based on considerations other than the study protocol, it is not a true
  experiment} \citep{rothman2012epidemiology}

Reinforcement learning can be seen as a form if
interventional data collection where intervention probabilities are known
\cite{bareinboim2015bandits}


Interventional data contains interaction with the patients / environment where the intervention probabilities are known.
Eg. RCT : fixed probability for any patient (statistical unit): first RCT, RCTs as the basis of Evidence Based Medicine in the late 80s.
Pbs of external validity raised in economics (Deaton, cf intro de la revue de Bénédicte). Focus is such problems is probably greater in economics because situations are not well controlled at all: very far from labs settings. Clinical situation is closer to lab (clinical epidemiology != social epidemiology)
What about medico-economics. I do not address this question in the present thesis, but it is a clear motivation
Eg. Conditional probability are interventional as well: conditionally randomized experiments in epidemiology (hernan), reinforcement learning in ML community (bareinboim2015bandits).

Observational data cannot intervene in any way with the patients. It should rely on observation alone to estimate intervention probability.

Distinction between association and causation (ladder of causation): first insight that different tools than statistics are needed.

\subsection{Focus of this thesis: real world data, observational}\label{subsec:intro:focus_data}

Rationnal:
Inconvenient of interventional data : Increasing costs of RCTs,
New challenges in public health: resource constraint, chronic disease,
Rise of digitalization and computing power allowing new research methods


\section{Various sources of data}\label{sec:intro:sources}

\subsection{Claims}\label{subsec:intro:claims}

Billing system, eg. PMSI.

Advantages (space and time coverage, scale, structure), disadvantages (not clinic, no exam results, few measures, heterogeneity of collection)

\subsection{EHRs and Hospital Information System}\label{subsec:intro:EHRs}

Increasing informatization
EHR at the center
Other applications part of HIS: give examples.

\subsection{Clinical Data Warehouse}\label{subsec:intro:cdw}
An infrastructure is needed to pool data from one or more medical information systems.
Definition of EHR: Health data warehouses (HDWs) refer to the pooling of data from one or more medical information systems, in a homogeneous format for reuse in management, research or care.


The 4 phases of data flow from the various sources that make up the HIS (figure)
Focus of this thesis: Clinical Data Warehouse and EHR, though most of my work should apply to claims

\section{Two cultures of statistics for health}\label{sec:two_cultures}

Statistical Learning Framework
General framework
Model selection
\subsection{One choice of perspective: Recent statistical learning}\label{subsec:intro:recent_statistical_learning}
% ML in healthcare 
Machines read medical images faster
and more efficiently than most practitioners \citep{zhou2021review}.
Structured data from Electronic Health Records \citep{rajkomar2018scalable} or
administrative databases \citep{beaulieu2021machine} outperform rule-based
clinical scores in predicting patient's readmission, in-hospital mortality or
future comorbidities \citep{li2020behrt}. Recently, large language models
leveraged clinical notes from several hospitals for length of stay prediction
\citep{jiang2023health}. Hope is high that LLMs models will soon be able to
help practitioners during consultation \citep{lee2023benefits}.



\section{Notions of causality}\label{sec:intro:causality}




\section{Overview and contributions}\label{sec:intro:contributions}



\chapter{Potential and challenges of Clinical Data Warehouse, a case study in France}\label{chapter:cdw}

\section{Abstract}\label{sec:cdw:abstract}
\section{Motivation and background: a changing world}\label{sec:cdw:motivation}


\subsection{Routine data collection are drawing increasing interest}\label{chapter:cdw:routine_data_collection}

\subsection{Different kind of healthcare data usages}\label{subsec:cdw:data_usages}

\paragraph{Primary data usages}
Primary usages directly serve one patient care.

\paragraph{Secondary data usages}
Secondary usages do not concern directly the care and support of one patient: research, quality or management indicators, billings.

\paragraph{Mix usages}

Mix usages such as learning algorithms

Health Technology A agencies interest: organizational impact of health products, potential for real world efficiency (entire life cycle of the product), quality, security, pertinence and security of care, health monitoring, health management.

Precision medicine

Fundamental research : biomarkers, observational epidemiology

\subsection{Healthcare data collection is heavily influenced from the local healthcare organization}

\paragraph{Centralized data collections: ex. Israël, HIRA}

\paragraph{More heterogeneous data collections structured into networks} ex. from

US, Korea, Germany,

\paragraph{The case of France}

centralized national insurer but scattered hospital ehrs. Projects in other areas: eg. cancer (unicancer), gp (cnge project, darmoni). National initiative to develop and structure hospital cdws.


\section{Methods: Which data usage in French University Hospitals}\label{sec:cdw:methods}

\subsection{Interviews and study coverage}\label{subsec:cdw:interviews}

\paragraph{Semi-structured interviews}

Topics, questions, link to full data and questionnaires in appendix.

\paragraph{Regional and university hospitals in France: different levels of maturity}

Scope of 32 CHUs, out of the 3000 care sites in France to yield exhaustive
conclusion on a restricted scope. Drive most specialized care, research in their
core mission. Date of interview

\paragraph{Focus on the 18 CDWs with highest level of maturity}

The denominator for the quantitative results is the 18 CDWs in production

\subsection{A classification of observational studies}\label{subsec:cdw:classification}

Contrast with classical epidemiology study types and the notion of experiment \citep{rothman2012epidemiology}.

\paragraph{Outcome frequency}
\paragraph{Population characterization}
\paragraph{Risk factors}
\paragraph{Treatment effect}
\paragraph{Development of diagnostic or prognostic algorithms}
\paragraph{Medical informatics}


\section{Results: A rapidly evolving landscape with a variety of
  situations}\label{sec:cdw:results}

\subsection{Governance}\label{subsec:cdw:results:governance}
\paragraph{Initiation and actors}
Figure temporality of CDW
Federating potential
Multiple departments involved
Multiple skills involved, strong ties with the academics
In-house solution development vs industrialization

\paragraph{Management of studies}

Scientific committee and project follow-up platform

\subsection{Transparency}\label{subsec:cdw:results:transparency}
Uneven public reference on hospital websites of ongoing studies.

\subsection{Data}\label{subsec:cdw:results:data}

\paragraph{Strong dependance to the HIS}

Data collected reflect data collection
Exemples, AP-HP, HCLs

\paragraph{Categories of Data}

Main functionalities of HIS are the same. Common base Details: Table Takeaway:
most of the current accessible data are billing, administrative and text,
importantly, low access to temporality.

\paragraph{Data reuse: research}
Details of current study types.
Details of specialty of the principal investigator
Interest for research data network but lack of resources

\paragraph{Data reuse: CDW are used for monitoring and management}
Initialization for billing
Potential for professional feedbacks
Pharmacovigilance

\paragraph{Data reuse: Strong interest for CDW in the context of care}

Some CDWs develop specific applications that provide new functionalities
compared to care software. Search engines can be used to query all the
hospital's data gathered in the CDW, without data compartmentalization between
different softwares. Dedicated interfaces can then offer a unified view of the
history of a patient's data, with inter-specialty transversality, which is
particularly valuable in internal medicine. These cross-disciplinary search
tools also enable healthcare professionals to conduct rapid searches in all the
texts, for example, to find similar patients \citep{garcelon2017finding}.
%
There is a growing interest in such computational phenotyping tools to support
the development of digital health solutions \citep{wen2023impact}.
%
Uses for prevention, automation of repetitive tasks, and care coordination are
also highlighted. Concrete examples are the automatic sorting of hospital
prescriptions by order of complexity or the setting up of specialized channels
for primary or secondary prevention.

\subsection{Technical architecture}\label{subsec:cdw:results:architecture}
Three layer : Data preprocessing (acquisition and normalization), storage, exposure
Datalab: a crucial technological brick

\subsection{Data quality, standard formats}\label{subsec:cdw:results:data_quality}
\paragraph{Quality tools}
Automatic tooling for acquisition
First development for automatic data checks
\paragraph{Standard format}
No single standard data model,
Omop
eHop
\paragraph{Documentation}
Half of the CDWs have put in place documentation accessible within the organization
No documentation is public

\section{Recommendations: What is needed to consolidate EHRs and their usages}\label{sec:cdw:recommendations}

\subsection{Governance}\label{subsec:cdw:recommendations:governance}
CDW becomes an essential component of data management in the hospital
Resources specific to the warehouse are rare and only project-based. Should promote long-middle term teams (eg. inria ?)
Multi-layered governance
\subsection{Transparency}
Public registration of comparative observational study protocols for research
Patient information stays limited
\subsection{Data}
study design : change of focus from data collection to data preprocessing -> other complementary skills needed
Link with the HIS, lack of standard at the HIS level, lack of sharing of data schema
Data reuse oriented towards primary care is still rare and rarely supported by appropriate funding.


\subsection{Technical architecture}
Lack of harmonization: focus on fewer solutions
Commercial solutions emerging
Case for open source: transparency, less technological lock-in, mutualization, favor modularity, help build consensus
Opportunity for open source solutions
Data quality, standard formats
Quality is not sufficiently considered as a relevant scientific topic itself.
Tooling: Link with devops/ automated CI in industrial data science
there is a need for open-source publication of research code to ensure quality retrospective research

\section{Conclusion}
The French CDW ecosystem is beginning to take shape
The priority is the creation and perpetuation of multidisciplinary warehouse teams
Constitution of a multilevel collaboration network is another priority.
Common data model should be encouraged
The question of expanding the scope of the data beyond the purely hospital domain must be asked.


\chapter{Exploring a complexity gradient in representation and predictive algorithms for EHRs}\label{chapter:predictive_models}
\section{Abstract}\label{sec:predictive_models:abstract}

\section{Motivation}\label{sec:predictive_models:motivation}

\subsection{Importance of predictive tasks from EHRs}\label{subsec:predictive_models:importance}
Prevention
Piloting
In the literature
Within french CDW (23 \% of studies, just after population definitions)

\subsection{EHRs data is complex}\label{subsec:predictive_models:complex_data}
How ? time, high cardinality, multi-modality
In the literature
Within french CDW(link with previous chapter)

\subsection{Low prevalence and local practice}\label{subsec:predictive_models:low_prevalence}

What are the most impactful algorithms for predictive tasks from structured EHR ?
A wealth of methods, but a lack of insights on the advantages and inconvenience for specific problems and resources.

\subsection{What makes a healthcare predictive model useful?}\label{subsec:predictive_models:useful}

We can share it
Performances
Insertion in the care workflow


\section{Predictive pipelines}\label{sec:predictive_models:pipelines}

Demographic features
Count encoding with event features
Static Embeddings of event features
Transformer based
\section{Empirical Study}\label{sec:predictive_models:empirical_study}
\subsection{Evaluation pipeline}\label{subsec:predictive_models:evaluation_pipeline}
\subsection{Task definitions}\label{subsec:predictive_models:task_definitions}

\paragraph{Length Of Stay}
\paragraph{Diagnosis Prediction}
\paragraph{Major Adverse Cardiovascular Events}
\subsection{Results: performance-sample trade-offs}

\section{Conclusion}\label{sec:predictive_models:conclusion}

\chapter{Prediction is not all we need: Causal analysis of EHRs to ground decision making}\label{chapter:causal_tuto}
\section{Abstract}\label{sec:causal_tuto:abstract}

\section{Motivation : Healthcare is concerned with decision making, not mere prediction}\label{sec:causal_tuto:motivation}

\subsection{Predictive medicine currently suffers from biases}\label{subsec:causal_tuto:predictive_medicine_biases}
(shortcuts, population shifts). Racial, gender and under-served population biases raise concern on fairness.
\subsection{A key ingredient to ground data-driven decision making is causal thinking}

\subsection{The need for synthetic materials for practitioners}\label{subsec:causal_tuto:synthetic_materials}

The relevant concepts for causal inference are scattered in different literatures. A dedicated exposition to time-varying data in EHRs would help practitioners and data scientists that study them.
\subsection{Motivating example}\label{subsec:causal_tuto:motivating_example}


\section{Step-by-step framework for robust decision making from EHR data}\label{sec:causal_tuto:framework}

\subsection{Robust study design to avoid biases: Framing the question}
PICOT
Selection Bias
Immortal time bias

\subsection{Is the dataset sufficient to inform on the intervention: identification}
Causal graph Computing the causal effect of interest: Estimation Confounders
extractions Confounders aggregation Causal estimators Nuisance estimators
\subsection{Assessing the robustness of the hypothesis: Vibration or sensitivity
  analysis}\label{subsec:causal_tuto:vibration_analysis}

Sensitivity vs robustness

\subsection{Heterogeneity of treatment}
Interest of HTE
How to do HTE ? Final regression analysis.

\section{Application on MIMIC-IV}\label{sec:causal_tuto:application}

Emulated trial: Effect of albumin in combination with crystalloids compared to crystalloids alone on 28-day mortality in sepsis patients
Choice of the trial
Known effects

\subsection{Framing the question}\label{subsec:causal_tuto:framing_mimic}
\subsection{Identification}
Covariates and dag
\subsection{Estimation}
Confounders extractions
Confounders aggregation
Causal estimators
Nuisance estimators
\subsection{Vibration analysis}
Varying estimation choices:
Varying inclusion criteria: illustration of immortal time bias
\subsection{Heterogeneity of treatment}

\subsection{Discussion}\label{subsec:causal_tuto:discussion}

\chapter{How to select predictive models for causal inference?}\label{chapter:causal_model_selection}
\section{Abstract}\label{sec:causal_model_selection:abstract}

\section{Motivation}\label{sec:causal_model_selection:motivation}

Extending prediction to prescription requires causal model selection
Illustration: the best predictor may not estimate best causal effects
Prior work: model selection for outcome modeling (g-computation)
\section{Methods}\label{sec:causal_model_selection:methods}


\subsection{Notations}\label{subsec:causal_model_selection:notations}

\subsection{Model-selection risks, oracle and feasible}\label{subsec:causal_model_selection:risks}
\subsection{Causal model selection}\label{subsec:causal_model_selection:causal_model_selection}

The $\tau\mathrm{-risk}$ : an oracle error risk
Feasible error risks
Estimation and model selection procedure
\section{Theory: Links between feasible and oracle risks}\label{sec:causal_model_selection:theory}

\subsection{Upper bound of $\tau\mathrm{-risk}$ with
  $\mu\mathrm{-risk}_{IPW}$}\label{subsec:causal_model_selection:upper_bound}

\subsection{Reformulation of the R-risk as reweighted
  $\tau\mathrm{-risk}$}\label{subsec:causal_model_selection:r_risk_reformulation}

\subsection{Interesting special
  cases}\label{subsec:causal_model_selection:special_cases}

\section{Empirical Study}\label{sec:causal_model_selection:empirical_study}

\subsection{Caussim: Extensive simulation settings}\label{subsec:causal_model_selection:caussim}
\paragraph{Data Generation Process}

Family of candidate estimators

\subsection{Semi-simulated datasets}\label{subsec:causal_model_selection:semi_simulated}
\paragraph{Datasets}
\paragraph{Family of candidate estimators}
\paragraph{Nuisance estimators}

\subsection{Measuring overlap between treated and non treated}\label{subsec:causal_model_selection:overlap}

\subsection{Empirical results: factors driving good model selection across
  datasets}\label{subsec:causal_model_selection:empirical_results}


The $R\mathrm{-risk}$ is the best metric

Model selection is harder in settings of low population overlap

Nuisances can be estimated on the same data as outcome
models

Use 90\% of the data to estimate outcome models, 10\% to select them

\section{Discussion and conclusion}\label{sec:causal_model_selection:discussion}


\chapter{Conclusion}\label{chapter:conclusion}

Predictive medecine is hard

Modern healthcare burdens and costs are driven by chronic diseases where death
is not the only outcome of interest.

Focus on smaller rewards, on which experiments are easier to conduct and where
error is possible: it would allow better learning for decision making since we
can repeat more experiments (limit: signals could be highly delayed).

Text is pervasive, we are using it to communicate and to log most of our
information. We should leverage it more in the context of care (limit:
temporality is hard to capture but is a key aspect of causal inference).


\printbibliography

\appendix

\clearpage

\section{Causal Diagrams}

\end{document}
